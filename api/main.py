from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
import os
import json
import requests
import logging
from pydantic import BaseModel

app = FastAPI()

#Logger settings
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def get_tickers():
    logger.info("Beginning of 'get_tickers' functions")
    polygon_api_key = os.getenv("POLYGON_API_KEY")
    logger.info(f'API Key {polygon_api_key} loaded')
    if not polygon_api_key:
        raise ValueError("Polygon api key not found")
    url = "https://api.polygon.io/v2/reference/tickers"
    headers = {
        "Authorization": "Bearer DP4Up8S7xWmiVSYQqcJKiiaMod1peU58"
    }
    logger.info(f'headers: {headers}')
    params = {
        "apiKey": polygon_api_key
    }
    logger.info(f'params: {params}')
    response = requests.get(url, headers=headers, params=params)
    logger.info(f'response: {response}')
    if response.status_code == 200:
        data = response.json()
        return data
    else:
        raise HTTPException(status_code=500, detail="failed to fetch data")


@app.get("/")
async def read_root():
    logger.info("This route worked, is this showing up in docker terminal")
    print("This route worked, is this showing up in docker terminal")
    return {"message": "whatup boyz api workin lolz"}


@app.get("/polygon/tickers/")
async def get_symbols():
    logger.info("route being tested AAAAAAAAAAAAAAAAAAA")
    tickers = get_tickers()
    
    return {"tickers": tickers}

# if __name__ == "__main__":
#     import uvicorn
#     uvicorn.run(app, host="0.0.0.0", port=8000)

